                             <font size='5'> Message queuing </font>  
# Message queue:
Message queuing allows applications to communicate by sending the messages to each other. The message queue is a temporary storage which stores the necessary when the destination server is busy or not connected.Message queues helps a lot with ensuring that the  data is never lost.
The Message queues are used because of :
1.Redundancy   
2.Traffic Spikes  
3.Batch  
4.Asynchronous Messaging  
5.Transaction Ordering   
6.Improve Scalability  
7. Guarantee Transaction Occurs Once  
8.Break Larger Tasks into Many Smaller Ones  
9.Monitoring 